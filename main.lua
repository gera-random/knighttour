function love.load()
	boardsize = 8
	board = makeboard(boardsize)
	tour  = knighttour({3,3}, board)
	print(dumptour(tour))
	print(dumpboard(board))
end

function recalctour(mx, my)
	local ww, wh = love.graphics.getDimensions()
	local squarew, squareh = ww/boardsize, wh/boardsize
	local x = math.floor(mx / squarew) + 1
	local y = math.floor(my / squareh) + 1
	board = makeboard(boardsize)
	tour  = knighttour({x,y}, board)
	print(dumptour(tour))
	print(dumpboard(board))
end

function love.touchpressed(id, x, y, dx, dy, pressure)
	recalctour(x, y)
end

function love.mousereleased(mx, my, button)
	recalctour(mx, my)
end

function love.draw()
	local ww, wh = love.graphics.getDimensions()
	local squarew, squareh = ww/boardsize, wh/boardsize
	for x = 0, boardsize-1 do
		for y = 0, boardsize-1 do
			if (x + y) % 2 == 0 then
				love.graphics.setColor(1,1,1)
			else
				love.graphics.setColor(0,0,0)
			end
			love.graphics.rectangle("fill", x*squarew, y*squareh, squarew, squareh)
		end
	end

	love.graphics.setColor(1,0,0)
	love.graphics.setLineJoin('bevel')
	love.graphics.setLineWidth(math.min(ww, wh)/100)
	local tour_points = {}
	for i = 1, #tour do
		table.insert(tour_points, squarew * tour[i][1] - squarew/2)
		table.insert(tour_points, squareh * tour[i][2] - squareh/2)
	end
	love.graphics.line(tour_points)
end

function makeboard(size)
	local board = {}
	for y = 1,size do
		board[y] = {}
		for x = 1,size do
			board[y][x] = 0
		end
	end
	return board
end

function dump(o)
	if type(o) == 'table' then
		local s = '{ '
		for k,v in pairs(o) do
			if type(k) ~= 'number' then k = '"'..k..'"' end
			s = s .. '['..k..'] = ' .. dump(v) .. ','
		end
		return s .. '} '
	else
		return tostring(o)
	end
end

function countmoves(x, y, board)
	local bs = #board
	if x < 1 or x > bs or y < 1 or y > bs then return end
	local moves = 0
	local squares = {}
	if board[y-2] then
		if board[y-2][x-1] == 0 then moves = moves + 1; table.insert(squares, {x-1, y-2}) end
		if board[y-2][x+1] == 0 then moves = moves + 1; table.insert(squares, {x+1, y-2}) end
	end
	if board[y-1] then
		if board[y-1][x-2] == 0 then moves = moves + 1; table.insert(squares, {x-2, y-1}) end
		if board[y-1][x+2] == 0 then moves = moves + 1; table.insert(squares, {x+2, y-1}) end
	end
	if board[y+1] then
		if board[y+1][x-2] == 0 then moves = moves + 1; table.insert(squares, {x-2, y+1}) end
		if board[y+1][x+2] == 0 then moves = moves + 1; table.insert(squares, {x+2, y+1}) end
	end
	if board[y+2] then
		if board[y+2][x-1] == 0 then moves = moves + 1; table.insert(squares, {x-1, y+2}) end
		if board[y+2][x+1] == 0 then moves = moves + 1; table.insert(squares, {x+1, y+2}) end
	end
	return moves, squares
end

function knighttour(pos, board)
	local tour = {}
	local x,y = unpack(pos)
	local n = 1
	while true do
		tour[n] = {x,y}
		board[y][x] = n
		n = n + 1
		local moves, squares = countmoves(x, y, board)
		if moves == 0 or not moves then break end
		local minmoves = 9
		local minmoves_square = {}
		for i = 1,#squares do
			local square_moves = countmoves(squares[i][1], squares[i][2], board)
			if square_moves and square_moves < minmoves then
				minmoves = square_moves
				minmoves_square = squares[i]
			end
		end
		x,y = unpack(minmoves_square)
	end
	return tour
end

function dumpboard(board)
	local bs = #board
	local s = ''
	for y = 1,bs do
		for x = 1,bs do
			s = s .. board[y][x] .. '\t'
		end
		s = s .. '\n'
	end
	return s
end

function dumptour(tour)
	local len = #tour
	local s = tour[1][1]..tour[1][2]
	for i = 2,len do
		s = s .. '->' .. tour[i][1]..tour[i][2]
	end
	return s
end
